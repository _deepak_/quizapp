from django.shortcuts import render, HttpResponse, Http404, get_object_or_404, redirect, reverse
from django.template.loader import render_to_string
from django.contrib.messages import get_messages
from django.contrib import messages

from urllib.parse import urlencode

import QuizApp.models as quiz_app_models
import json
import time
import random


def homepage_view(request):
    """
        Renders homepage
    :param request:
    :return:
    """
    return render(request, 'QuizApp/home.html', context={})


def list_quizzes(request):
    """
        List all the available quizzes. or quizzes with matching search words.

    :param request:
    :return:
    """
    if request.method == 'GET':
        quiz_set = quiz_app_models.Quiz.objects.all()
        context = {
            'quiz_set': quiz_set,
        }
        return render(request, 'QuizApp/view_quizzes.html', context=context)
    elif request.method == 'POST':
        keywords = request.POST['keywords']


def add_quiz(request):
    """
    This function is responsible to add a new quiz.
    :param request:
    :return:
    """
    context = {
        "messages": get_messages(request),
    }
    return render(request, 'QuizApp/add_quiz.html', context=context)


def add_quiz_form(request):
    n_questions = 10
    n_options = 4

    if request.method == 'GET':
        context = {
            "qs_range": list(range(1, n_questions + 1)),
            "option_range": list(range(1, n_options + 1)),
            "messages": get_messages(request)
        }
        return render(request, 'QuizApp/add_quiz_form.html', context=context)
    elif request.method == 'POST':
        try:
            quiz_title = request.POST['quiz_title']
            quiz = quiz_app_models.add_quiz(title=quiz_title)
            print(request.POST)

            for qs_no in range(1, n_questions + 1):
                question_text = request.POST[f'option_text-{qs_no}']
                question = quiz_app_models.add_question(question_text=question_text, quiz=quiz)
                for opn in range(1, n_options + 1):
                    answer_text = request.POST[f'option_text-q{qs_no}-o{opn}']
                    is_correct = f'is_correct-q{qs_no}-o{opn}' in request.POST
                    quiz_app_models.add_answer(answer_text=answer_text, question=question, is_correct=is_correct)
            messages.success(request, 'Successfully created your Quiz')
            return redirect("QuizApp:home")
        except:
            messages.warning(request, "ERROR: Failed to create the Quiz.")


def add_trivia_quiz(request):
    if request.method == 'GET':

        context = {
            "messages": get_messages(request),
        }
        return render(request, 'QuizApp/trivia_quiz.html', context=context)
    elif request.method == 'POST':
        try:
            quiz_title = request.POST['title']
            api_url = request.POST['link']
            quiz_app_models.add_quizzes_from_trivia(query_link=api_url, title=quiz_title, n_quizzes=1)

            messages.success(request, 'Successfully added the Quiz')
            return redirect('QuizApp:add-trivia-quiz')
        except:
            messages.warning(request, 'ERROR: Failed to add the Quiz')
            return redirect('QuizApp:add-trivia-quiz')


def edit_quiz(request, pk):
    if request.method == 'GET':
        quiz = get_object_or_404(quiz_app_models.Quiz, pk=pk)

        context = {
            'quiz': quiz
        }
        return render(request, 'QuizApp/add_quiz.html', context=context)


def attempt_quiz(request, pk):
    if request.method == 'GET':
        quiz = get_object_or_404(quiz_app_models.Quiz, pk=pk)

        context = {
            'quiz': quiz
        }

        response_obj = render(request, 'QuizApp/attempt_quiz.html', context=context)
        # for key in request.COOKIES:
        #     response_obj.delete_cookie(key)

        response_obj.set_cookie("start_time", time.time())
        response_obj.set_cookie("QuizID", pk)
        response_obj.set_cookie("responses", {})
        response_obj.set_cookie("is_correct_array", {})
        return response_obj


def question_view(request, quiz_pk, qs_no):
    if request.method == 'GET':
        quiz = get_object_or_404(quiz_app_models.Quiz, pk=quiz_pk)

        if qs_no < 11:
            question = quiz.question_set.all()[qs_no - 1]
            options = list(question.answer_set.all())
            random.shuffle(options)
            context = {
                "quiz_pk": quiz_pk,
                "question": question,
                "options": options,
                "button_text": "Submit" if qs_no is 10 else "Next",
                "curr_qs_no": qs_no,
                "next_qs_no": qs_no + 1,
                "score": 0,
            }

            return render(request, 'QuizApp/question.html', context=context)

        elif qs_no == 11:
            return redirect("QuizApp:show-responses", quiz_pk)
            # return HttpResponse("Final submission implementation not complete.")
        else:
            return Http404()

    elif request.method == 'POST':
        quiz = get_object_or_404(quiz_app_models.Quiz, pk=quiz_pk)

        question = quiz.question_set.all()[qs_no - 1]

        submitted_answers = list(request.POST.keys())[1:]
        # response_obj = redirect('QuizApp:question_view', quiz_pk, qs_no + 1)
        response_obj = redirect('QuizApp:curr-response', quiz_pk, qs_no)

        curr_cookie = request.COOKIES
        curr_responses = json.loads(curr_cookie["responses"])
        curr_responses[str(qs_no)] = submitted_answers
        response_obj.set_cookie("responses", json.dumps(curr_responses))
        return response_obj


def get_color(is_selected, is_correct):
    if is_selected and is_correct:
        return 'green'
    elif not is_selected and not is_correct:
        return 'black'
    else:
        return 'red'


def show_current_response(request, quiz_pk, qs_no, return_type="http-response"):
    submitted_responses = json.loads(request.COOKIES["responses"])[str(qs_no)]
    quiz = get_object_or_404(quiz_app_models.Quiz, pk=quiz_pk)
    question = quiz.question_set.all()[qs_no - 1]
    correct_responses = [str(answer.pk) for answer in question.answer_set.all() if answer.is_correct]
    correct_submission = (sorted(submitted_responses) == sorted(correct_responses))
    if qs_no is 10:
        end_time = time.time()

    context = {
        "quiz_pk": quiz_pk,
        "question": question,
        "is_answered": True,
        "curr_qs_no": qs_no,
        "next_qs_no": qs_no + 1,
        "button_text": "Next",
        "options": [(option, str(option.pk) in submitted_responses, str(option.pk) in correct_responses,
                     get_color(str(option.pk) in submitted_responses, str(option.pk) in correct_responses))
                    for option in question.answer_set.all()],
        "correct_submission": correct_submission,
        "correct_options": [option for option in question.answer_set.all() if option.is_correct],
        "return_string": return_type == 'string',
    }
    if return_type == 'string':
        return render_to_string('QuizApp/response_view.html', context=context)

    response_obj = render(request, 'QuizApp/quiz_response.html', context=context)

    new_correct_array = json.loads(request.COOKIES["is_correct_array"])
    new_correct_array[str(qs_no)] = correct_submission

    response_obj.set_cookie("is_correct_array", json.dumps(new_correct_array))

    if qs_no is 10 and (
            'end_time' not in request.COOKIES or request.COOKIES['end_time'] < request.COOKIES["start_time"]):
        response_obj.set_cookie('end_time', end_time)
    return response_obj


def show_all_response(request, quiz_pk):
    # submitted_responses = json.loads(request.COOKIES["responses"])
    html_response = ""

    # quiz = get_object_or_404(quiz_app_models.Quiz, pk=quiz_pk)
    for qs_no in range(1, 11):
        html_response += "\n" + show_current_response(request, quiz_pk, qs_no, return_type='string')

    start_time = float(request.COOKIES['start_time'])
    end_time = float(request.COOKIES['end_time'])
    # time taken
    minutes, seconds = divmod((int(end_time - start_time)), 60)

    score = sum(map(lambda x: int(x), json.loads(request.COOKIES["is_correct_array"]).values()))

    context = {
        "html_response": html_response,
        "minutes": minutes,
        "seconds": seconds,
        "score": score,
    }
    response_obj = render(request, 'QuizApp/final_response.html', context=context)
    return response_obj


def save_quiz_response(request):
    if request.method == 'GET':
        return render(request, 'QuizApp/user_form.html')
    if request.method == 'POST':
        name = request.POST["name"]
        email = request.POST["email"]

        curr_cookie = request.COOKIES
        del curr_cookie['csrftoken']
        quiz = get_object_or_404(quiz_app_models.Quiz, pk=curr_cookie["QuizID"])
        quiz_response_obj = quiz_app_models.QuizResponse(name=name, email=email, Quiz=quiz, response_json=curr_cookie)
        quiz_response_obj.save()

        return render(request, 'QuizApp/user_form.html', {"saved": True})

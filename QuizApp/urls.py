from django.urls import path
import QuizApp.views as quiz_app_views

app_name = "QuizApp"
urlpatterns = [
    path('', quiz_app_views.homepage_view, name='home'),
    path('quizzes/', quiz_app_views.list_quizzes, name='list_quizzes'),
    path('add-quiz/', quiz_app_views.add_quiz, name='add_quiz'),
    path('quiz-form/', quiz_app_views.add_quiz_form, name='add_quiz_form'),
    path('edit-quiz/<int:pk>/', quiz_app_views.edit_quiz, name='edit_quiz'),
    path('attempt-quiz/<int:pk>/', quiz_app_views.attempt_quiz, name='attempt_quiz'),
    path('quiz/<int:quiz_pk>/<int:qs_no>/', quiz_app_views.question_view, name='question_view'),
    path('save-responses/', quiz_app_views.save_quiz_response, name='save-responses'),
    path('show-responses/<int:quiz_pk>', quiz_app_views.show_all_response, name='show-responses'),
    path('curr-response/<int:quiz_pk>/<int:qs_no>/', quiz_app_views.show_current_response, name='curr-response'),
    path('add_trivia_quiz/', quiz_app_views.add_trivia_quiz, name='add-trivia-quiz')
]

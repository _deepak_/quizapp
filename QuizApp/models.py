from django.db import models
import json
from urllib.request import urlopen


# https://opentdb.com/api.php?amount=10&difficulty=easy&type=multiple

class Quiz(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Question(models.Model):
    QtypeChoices = [("Boolean", 0), ("Single_Answer", 1), ("Multiple_Answer", 2)]

    question_text = models.CharField(max_length=1000)
    question_type = models.IntegerField(choices=QtypeChoices, default=2)

    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

    def __str__(self):
        return self.question_text


class Answer(models.Model):
    answer_text = models.CharField(max_length=1000)
    is_correct = models.BooleanField()

    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.answer_text


class QuizResponse(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    Quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    # response_json = models.JSONField()
    response_json = models.CharField(max_length=10000)


def add_answer(answer_text, question, is_correct=False):
    ans = Answer(answer_text=answer_text, question=question, is_correct=is_correct)
    ans.save()
    return ans


def add_question(question_text, quiz, question_type=2):
    ques = Question(question_text=question_text, quiz=quiz, question_type=question_type)
    ques.save()
    return ques


def add_quiz(title):
    quiz = Quiz(title=title)
    quiz.save()
    return quiz


def add_quizzes_from_trivia(query_link, title=None, n_quizzes=1):
    """
    This Function is responsible to add new Quizzes using the questions downloaded from
    trivia API and write it to the database.

    :link: A link which will return json response with the list of questions.
           (i.e: https://opentdb.com/api.php?amount=500&difficulty=easy&type=multiple)

    :return:  None
    """

    data = json.loads(urlopen(query_link).read())['results'][: n_quizzes * 10]
    qs_count = 0
    for Q in data:
        if qs_count % 10 == 0:
            if not title:
                quiz = add_quiz(title=f"Quiz No: {qs_count // 10}")
            else:
                quiz = add_quiz(title=title)
        qs_count += 1

        question = add_question(Q['question'], quiz=quiz)
        add_answer(answer_text=Q['correct_answer'], question=question, is_correct=True)
        for ans in Q['incorrect_answers']:
            add_answer(answer_text=ans, question=question)

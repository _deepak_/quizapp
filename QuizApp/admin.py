from django.contrib import admin
import QuizApp.models as quiz_app_models

admin.site.register(quiz_app_models.Quiz)
admin.site.register(quiz_app_models.Question)
admin.site.register(quiz_app_models.Answer)
